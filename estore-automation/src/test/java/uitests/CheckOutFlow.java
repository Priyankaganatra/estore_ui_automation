package uitests;
//import org.apache.http.entity.mime.Header;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import pageobjects.BillingInfoPage;
import pageobjects.Header;
import pageobjects.PaymentInfoPage;
import pageobjects.ProductPage;
import pageobjects.QknSignIn;
import pageobjects.SignIn;
import pageobjects.SiteSelectDropdown;
import properyreader.ConfigProperty;
import properyreader.PropertyReader;
import runtime.RuntimeParameter;
import runtime.RuntimeParameterReader;
import validators.HomePageValidator;
import validators.SignInPageValidator;

public class CheckOutFlow extends UIBaseTest {

	@Test
	public void testPositiveScenarioEndToEnd() throws InterruptedException {

		// step 1: Load home page

		WebDriver driver = new ChromeDriver();
		driver.get(PropertyReader.getValue(ConfigProperty.HomePageURL));
		// need a better way to wait till page load is complete
		Thread.sleep(2000);
		HomePageValidator homePageValidator = new HomePageValidator(driver);
		homePageValidator.validateHighLevelComponents();

		// step 2: select site
		SiteSelectDropdown siteSelectDropdown = new SiteSelectDropdown(driver);
		siteSelectDropdown.selectSite(RuntimeParameterReader
				.getRuntimeParamValue(RuntimeParameter.site));

		Thread.sleep(2000);

		// validate whether the site was updated or not
		homePageValidator.validateSelectedSite();
		// SignIn Module :

		Header header = new Header(driver);
		// validate header module.
		header.clickOnMyAccount();

		Thread.sleep(20000);

		QknSignIn qknSignInModule = new QknSignIn(driver);
		qknSignInModule.enterUserName("iamtestpass+testPreprod01@gmail.com")
				.clickOnSignInButton();

		Thread.sleep(6000);

		SignIn signInModule = new SignIn(driver);

		SignInPageValidator signInPageValidator = new SignInPageValidator(
				driver);
		signInPageValidator.signInPageObject(signInModule)
				.validateSignInPageBasics();
		// validate basics first
		// read the username and password from configs and data-providers
		signInModule.enterUserName("iamtestpass+testPreprod01@gmail.com")
				.enterPassword("Quicken123!").clickOnSignInButton();
		// validate the page after successful sign in
		Thread.sleep(8000);
		header.clickOnQuickenLogo();
		Thread.sleep(2000);

		// step 3: select product

		header.clickOnQuickenLogo();
		Thread.sleep(4000);

		header.selectHomeAndBusinessProduct();
		Thread.sleep(4000);

		// step 4: click on subscribe now
		ProductPage productPageModule = new ProductPage(driver);
		productPageModule.clickOnSubscribeNowButton();
		Thread.sleep(4000);

		// step 5: provide all required info
		
		PaymentInfoPage paymentInfoPageModule = new PaymentInfoPage(driver);
		 paymentInfoPageModule.enterCardNumber("4111111111111111");
		 paymentInfoPageModule.enterExpiration("04/19");
		 paymentInfoPageModule.enterSecurityCode("123");
		 paymentInfoPageModule.canadaEnterNameOnCard("Priyanka Ganatra");
		 
//		BillingInfoPage billingInfoPageModule = new BillingInfoPage(driver);
//		billingInfoPageModule.enterAddress("2 bloor st");
//		billingInfoPageModule.enterCity("Toronto");
//		billingInfoPageModule.enterState("Ontario");
//		billingInfoPageModule.enterZipcode("M4W 3E2");
//		
			//	Thread.sleep(2000);
		
		
		// Payment Info Page :
		 
		 //paymentInfoPageModule.clickOnContinueToRevieOrderButton();
//		 Thread.sleep(4000);
//		 billingInfoPageModule.clickOnContinueToPaymentInfoButton();

		// step 6: payment info

		// step 7: review/place order

		// step 8: get confirmation and download product
		driver.quit();
	}

}
