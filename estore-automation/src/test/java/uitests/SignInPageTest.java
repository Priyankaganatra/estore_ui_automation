package uitests;
import java.net.MalformedURLException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import pageobjects.BillingInfoPage;
import pageobjects.Header;
import pageobjects.OrderConfirmationPage;
import pageobjects.PaymentInfoPage;
import pageobjects.ProductPage;
import pageobjects.QknSignIn;
import pageobjects.ReviewOrderPage;
import pageobjects.SignIn;
import properyreader.ConfigProperty;
import properyreader.PropertyReader;
import util.Browser;
import util.BrowserName;
import validators.SignInPageValidator;

public class SignInPageTest extends UIBaseTest {

	// data-providers
	@Test
	public void testSignInPageBasics() throws InterruptedException, MalformedURLException {
		WebDriver driver = Browser.getDriver(BrowserName.Chrome);
		//driver.get(PropertyReader.getValue(ConfigProperty.HomePageURL));
		driver.get("https://qkn:apple@quickenpreprod.prod.acquia-sites.com");

		
		// need a better way to wait till page load is complete
		Thread.sleep(8000);
		Header header = new Header(driver);
		// validate header module.
		header.clickOnMyAccount();
		
		Thread.sleep(7000);
		
		QknSignIn qknSignInModule = new QknSignIn(driver);
		qknSignInModule.enterUserName("iamtestpass+testPreprod01@gmail.com")
		.clickOnSignInButton();

		Thread.sleep(6000);
		
		SignIn signInModule = new SignIn(driver);
		
		SignInPageValidator signInPageValidator = new SignInPageValidator(driver);
		signInPageValidator.signInPageObject(signInModule).validateSignInPageBasics();
		// validate basics first
		// read the username and password from configs and data-providers
		signInModule.enterUserName("iamtestpass+testPreprod01@gmail.com")
				.enterPassword("Quicken123!").clickOnSignInButton();
		// validate the page after successful sign in
		Thread.sleep(4000);
		header.clickOnQuickenLogo();
		Thread.sleep(2000);
		header.selectStarterProduct();
			//driver.get("https://quickenpreprod.prod.acquia-sites.com/personal-finance/quicken-starter-edition-2017");
			Thread.sleep(2000);
			
			// Prouduct Page : Need to put validators :
			
			ProductPage productPageModule = new ProductPage(driver);
			
			productPageModule.clickOnAddToCartButton();
			Thread.sleep(2000);
			
			
			// Billing Info Page :
			BillingInfoPage billingInfoPageModule = new BillingInfoPage(driver);
			billingInfoPageModule.enterFirstName("Priyanka");
			billingInfoPageModule.enterLastName("Ganatra");
			billingInfoPageModule.enterAddress("4359 sloat rd;");
			billingInfoPageModule.enterCity("Freomnt");
			billingInfoPageModule.enterState("CA");
			billingInfoPageModule.enterZipcode("94538");
			
			billingInfoPageModule.clickOnContinueToPaymentInfoButton();
			Thread.sleep(2000);
			
			
			// Payment Info Page :
			 PaymentInfoPage paymentInfoPageModule = new PaymentInfoPage(driver);
			 paymentInfoPageModule.enterCardNumber("4111111111111111");
			 paymentInfoPageModule.enterExpiration("04/19");
			 paymentInfoPageModule.enterSecurityCode("123");
			 paymentInfoPageModule.enterNameOnCard("Priyanka Ganatra");
			 
			 paymentInfoPageModule.clickOnContinueToRevieOrderButton();
			 Thread.sleep(4000);
			 
			 // Review Order Page:
			 
			 ReviewOrderPage reviewOrderModule = new ReviewOrderPage(driver);
			 reviewOrderModule.clickOnPlaceOrderButton();
			 Thread.sleep(8000);
			 
			// Order Confirmation Page:
			 
			 OrderConfirmationPage orderConfirmationModule = new OrderConfirmationPage(driver);
			 orderConfirmationModule.clickOnPlaceOrderButton();
			 Thread.sleep(8000);
				
		driver.quit();
	}

	
	@Test
	public void testConfigs() {
		//UserIdLabel
		System.out.println(PropertyReader.getValue(ConfigProperty.ChangingProperty));
	}
}
