package pageobjects;

import locators.PaymentInfoPageLocator;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class PaymentInfoPage extends PageObject {

	public PaymentInfoPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public PaymentInfoPage enterCardNumber(String cardNumber) {
			WebElement cardNumberElement = driver.findElement(By.
					xpath(PaymentInfoPageLocator.cardNumber));
			//cardNumberElement.clear();
			
			action.sendKeys(cardNumberElement, cardNumber).perform();
			return this;
		}
	
	public PaymentInfoPage enterExpiration(String expiration) {
		WebElement expirationElement = driver.findElement(By.
				xpath(PaymentInfoPageLocator.expiration));
		//expirationElement.clear();
		action.sendKeys(expirationElement, expiration).perform();
		return this;
	}
	
	public PaymentInfoPage enterSecurityCode(String securityCode) {
		WebElement securityCodeElement = driver.findElement(By.
				xpath(PaymentInfoPageLocator.securityCode));
		//securityCodeElement.clear();
		action.sendKeys(securityCodeElement, securityCode).perform();
		return this;
	}
	
	public PaymentInfoPage enterNameOnCard(String nameOnCard) {
		WebElement nameOnCardElement = driver.findElement(By.
				xpath(PaymentInfoPageLocator.nameOnCard));
		//nameOnCardElement.clear();
		action.sendKeys(nameOnCardElement, nameOnCard).perform();
		return this;
	}
	public PaymentInfoPage canadaEnterNameOnCard(String nameOnCard) {
		WebElement nameOnCardElement = driver.findElement(By.
				xpath(PaymentInfoPageLocator.candaNameOnCard));
		//nameOnCardElement.clear();
		action.sendKeys(nameOnCardElement, nameOnCard).perform();
		return this;
	}
	public PaymentInfoPage clickOnContinueToRevieOrderButton() {
			WebElement continueToRevieOrderButtonElement = driver.findElement(By
					.xpath(PaymentInfoPageLocator.continueToRevieOrderButton));
			action.click(continueToRevieOrderButtonElement).perform();
			return this;
		}
	
}
