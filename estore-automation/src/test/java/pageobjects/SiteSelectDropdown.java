package pageobjects;

import java.util.List;

import locators.HomePageLocator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SiteSelectDropdown extends PageObject {

	private WebElement siteSelector;

	public SiteSelectDropdown(WebDriver driver) {
		super(driver);
		siteSelector = driver.findElement(By
				.xpath(HomePageLocator.siteSelector));
	}

	public boolean selectSite(String siteName) {
		boolean isFound = false;
		// Check if the currently selected site if the given site or not
		if (this.getSelectedSite().equals(siteName)) {
			// do nothing
			System.out.println("Site " + siteName + " is already selected");
			isFound = true;
		} else {
			this.action.moveToElement(siteSelector).perform();
			List<WebElement> entries = siteSelector.findElement(
					By.xpath(HomePageLocator.optionSublistWithingSiteSelector))
					.findElements(By.xpath("li/a"));
			for (WebElement webElement : entries) {
				if (!isFound) {
					if (webElement.getText().equals(siteName)) {
						webElement.click();
						isFound = true;
					}
				}
			}
		}
		return isFound;
	}

	public String getSelectedSite() {
		return siteSelector.findElement(
				By.xpath(HomePageLocator.selectedSiteEntryWithinSiteSelector))
				.getText();
	}
}
