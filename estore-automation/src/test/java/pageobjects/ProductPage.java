package pageobjects;

import locators.ProductPageLocator;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ProductPage extends PageObject{

	public ProductPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	public ProductPage clickOnAddToCartButton() {
		WebElement addToCartButtonElement = driver.findElement(By
				.xpath(ProductPageLocator.addToCartButton));
		action.click(addToCartButtonElement).perform();
		return this;
	}
	
	public ProductPage clickOnSubscribeNowButton() {
		WebElement subscribeNowButtonElement = driver.findElement(By
				.xpath(ProductPageLocator.subscribeNowButton));
		action.click(subscribeNowButtonElement).perform();
		return this;
	}
	
	
	
}
