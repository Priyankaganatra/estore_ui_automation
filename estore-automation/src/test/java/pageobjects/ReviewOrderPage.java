package pageobjects;

import locators.ProductPageLocator;
import locators.ReviewOrderPageLocator;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ReviewOrderPage extends PageObject {

	public ReviewOrderPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	public ReviewOrderPage clickOnPlaceOrderButton() {
		WebElement placeOrderButtonElement = driver.findElement(By
				.xpath(ReviewOrderPageLocator.placeOrderButton));
		action.click(placeOrderButtonElement).perform();
		return this;
	}

}
