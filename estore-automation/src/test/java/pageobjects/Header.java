package pageobjects;

import locators.HomePageLocator;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Header extends PageObject {
	
	public Header(WebDriver driver) {
		super(driver);
	}
	
	public void clickOnMyAccount() {
		WebElement element = driver.findElement(By.xpath(HomePageLocator.signInButton));
		action.moveToElement(element).perform();
		WebElement myAccountElement = driver.findElement(By
				.xpath(HomePageLocator.myAccountXpath));
		action.click(myAccountElement).perform();
	}
	
	public void clickOnQuickenLogo() {
		WebElement element = driver.findElement(By.xpath(HomePageLocator.quickenLogo));
		action.moveToElement(element).perform();
		action.click(element).perform();
		
	}
	

	public void selectStarterProduct() {
		// TODO Auto-generated method stub
		WebElement element = driver.findElement(By.xpath(HomePageLocator.productMenu));
		action.moveToElement(element).perform();
		WebElement productElement = driver.findElement(By
				.xpath(HomePageLocator.starterEditionItem));
		action.click(productElement).perform();
	}

	public void selectHomeAndBusinessProduct() {
		// TODO Auto-generated method stub
		WebElement element = driver.findElement(By.xpath(HomePageLocator.canadaProductMenu));
		action.moveToElement(element).perform();
		WebElement productElement = driver.findElement(By
				.xpath(HomePageLocator.homeAndBusinessItem));
		action.click(productElement).perform();
	}
}
