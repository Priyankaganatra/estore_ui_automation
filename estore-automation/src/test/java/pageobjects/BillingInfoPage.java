package pageobjects;

import locators.BillingInfoPageLocator;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class BillingInfoPage extends PageObject {

	public BillingInfoPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
		
	public BillingInfoPage enterFirstName(String firstName) {
		WebElement firstNameElement = driver.findElement(By
				.xpath(BillingInfoPageLocator.firstName));
		firstNameElement.clear();
		action.sendKeys(firstNameElement, firstName).perform();
		return this;
	}

	
	public BillingInfoPage enterLastName(String lastName) {
		WebElement lastNameElement = driver.findElement(By
				.xpath(BillingInfoPageLocator.lastName));
		lastNameElement.clear();
		action.sendKeys(lastNameElement, lastName).perform();
		return this;
	}
	
	
	public BillingInfoPage enterAddress(String address) {
		WebElement addressElement = driver.findElement(By
				.xpath(BillingInfoPageLocator.address));
		addressElement.clear();
		action.sendKeys(addressElement, address).perform();
		return this;
	}
	
	public BillingInfoPage enterCity(String city) {
		WebElement cityElement = driver.findElement(By
				.xpath(BillingInfoPageLocator.city));
		cityElement.clear();
		action.sendKeys(cityElement, city).perform();
		return this;
	}

	public BillingInfoPage enterState(String state) {
		WebElement stateElement = driver.findElement(By
				.xpath(BillingInfoPageLocator.state));
		//stateElement.clear();
		action.sendKeys(stateElement, state).perform();
		return this;
	}
	
	public BillingInfoPage enterZipcode(String zipcode) {
		WebElement zipcodeElement = driver.findElement(By
				.xpath(BillingInfoPageLocator.zipcode));
		zipcodeElement.clear();
		action.sendKeys(zipcodeElement, zipcode).perform();
		return this;
	}

	public BillingInfoPage clickOnContinueToPaymentInfoButton() {
		WebElement continueToPaymentInfoButtonElement = driver.findElement(By
				.xpath(BillingInfoPageLocator.continueToPaymentInfoButton));
		action.click(continueToPaymentInfoButtonElement).perform();
		return this;
	}

}
