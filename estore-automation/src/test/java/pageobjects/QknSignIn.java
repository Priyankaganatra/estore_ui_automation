package pageobjects;

import locators.QknSignInLocators;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class QknSignIn extends PageObject {
	public QknSignIn(WebDriver driver) {
		super(driver);
	}

	public QknSignIn enterUserName(String userName) {
		WebElement userIdElement = driver.findElement(By
				.xpath(QknSignInLocators.qknUserIdXPath));
		action.sendKeys(userIdElement, userName).perform();
		return this;
	}

	
	public QknSignIn clickOnSignInButton() {
		WebElement signInButtonElement = driver.findElement(By
				.xpath(QknSignInLocators.qknSignInButtonXPath));
		action.click(signInButtonElement).perform();
		return this;
	}

	
	
}
