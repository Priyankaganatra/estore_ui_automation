package pageobjects;

import locators.SignInLocators;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SignIn extends PageObject {

	public SignIn(WebDriver driver) {
		super(driver);
	}

	public SignIn enterUserName(String userName) {
		WebElement userIdElement = driver.findElement(By
				.xpath(SignInLocators.userIdXPath));
		action.sendKeys(userIdElement, userName).perform();
		return this;
	}

	public SignIn enterPassword(String password) {
		WebElement passwordElement = driver.findElement(By
				.xpath(SignInLocators.passwordXPath));
		action.sendKeys(passwordElement, password).perform();
		return this;
	}

	public SignIn clickOnSignInButton() {
		WebElement signInButtonElement = driver.findElement(By
				.xpath(SignInLocators.signInButtonXPath));
		action.click(signInButtonElement).perform();
		return this;
	}

	public String getUserNameLabel() {
		WebElement userIdLabel = driver.findElement(By
				.xpath(SignInLocators.userIdLabel));
		return userIdLabel.getText();
	}

	public String getPasswordLabel() {

		return null;
	}

	public String signInLabel() {

		return null;
	}

}
