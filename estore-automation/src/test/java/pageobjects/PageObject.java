package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public abstract class PageObject {

	protected WebDriver driver;
	protected Actions action;
	
	PageObject(WebDriver driver) {
		this.driver = driver;
		action = new Actions(driver);
	}
}
