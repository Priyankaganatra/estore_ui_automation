package pageobjects;

import locators.OrderConfirmationPageLocator;
import locators.ReviewOrderPageLocator;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class OrderConfirmationPage extends PageObject {

	public OrderConfirmationPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public OrderConfirmationPage clickOnPlaceOrderButton() {
		WebElement downloadQuickenButtonElement = driver.findElement(By
				.xpath(OrderConfirmationPageLocator.downloadQuickenButton));
		action.click(downloadQuickenButtonElement).perform();
		return this;
	}
}
