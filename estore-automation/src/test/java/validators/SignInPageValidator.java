package validators;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import pageobjects.SignIn;

public class SignInPageValidator extends Validator {

	private SignIn signInPageObject;

	public SignInPageValidator(WebDriver driver) {
		super(driver);
	}

	public SignInPageValidator signInPageObject(SignIn signInPageObject) {
		this.signInPageObject = signInPageObject;
		return this;
	}

	public SignInPageValidator validateSignInPageBasics() {
		// Validate text for user-name
		// Validate text for password

		// null-check for page object
		if (null == signInPageObject)
			signInPageObject = new SignIn(driver);
		Assert.assertTrue(
				signInPageObject.getUserNameLabel().equalsIgnoreCase(
						"Email or Intuit ID"),
				"Expected user name label was: Email or Intuit ID but received: "
						+ signInPageObject.getUserNameLabel());

		return this;
	}

	public SignInPageValidator validateSomethingSpecific() {

		return this;
	}

}
