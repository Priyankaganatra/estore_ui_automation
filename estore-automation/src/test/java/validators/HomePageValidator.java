package validators;

import locators.HomePageLocator;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import pageobjects.SiteSelectDropdown;
import properyreader.ConfigProperty;
import properyreader.PropertyReader;
import runtime.RuntimeParameter;
import runtime.RuntimeParameterReader;
import util.Site;
import util.Utility;

public class HomePageValidator extends Validator {

	public HomePageValidator(WebDriver driver) {
		super(driver);

	}

	public HomePageValidator validateHighLevelComponents() {
		// do some validations here
		System.out.println("Validating high level components in home page");
		// check if header is there or not
		Assert.assertTrue(
				Utility.isWebElementPresent(driver,
						By.xpath(HomePageLocator.header)),
				"Header is not present");

		String site = RuntimeParameterReader
				.getRuntimeParamValue(RuntimeParameter.site);

		if (site.equals(Site.US.name)) {
			// US specific validations
		} else if (site.equals(Site.CANADA.name)) {
			// CA specific validations
		} else {
			// TODO
			System.out.println("No validation supported for the site: " + site);
		}
		return this;
	}
	
	public HomePageValidator validateSelectedSite() {
		// validation for selected site
		
		String expectedSite = PropertyReader.getValue(ConfigProperty.SiteNameInDropdown);
				
		SiteSelectDropdown siteSelectDropdown = new SiteSelectDropdown(driver);
		
		String actualSite = siteSelectDropdown.getSelectedSite();
		
		Assert.assertEquals(actualSite, expectedSite, "Expected : " + expectedSite + ", actual : " + actualSite);
		
		return this;
	}
}
