package validators;

import org.openqa.selenium.WebDriver;

public abstract class Validator {

	protected WebDriver driver;
	
	Validator(WebDriver driver) {
		this.driver = driver;
	}
}
