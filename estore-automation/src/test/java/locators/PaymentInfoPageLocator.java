package locators;

public class PaymentInfoPageLocator {

	
	public static String cardNumber = ".//*[@id='card-number-field']";
	public static String expiration = ".//*[@id='card-expiration-date-field']";
	public static String securityCode = ".//*[@id='card-security-code-field']";
	public static String nameOnCard = ".//*[@id='quicken-checkout-form']/div[3]/div[2]/div/div[1]";
	public static String continueToRevieOrderButton = ".//*[@id='quicken-checkout-form']/div[4]/input";
	
	// for Canada store :
	public static String candaNameOnCard = ".//*[@id='quicken-checkout-customer-info-combined-form']/div/div[4]/div/div/div[1]";
	
}
