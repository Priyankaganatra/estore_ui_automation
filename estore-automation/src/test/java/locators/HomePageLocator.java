package locators;

public class HomePageLocator {
	
	public static String myAccountXpath = "html/body/div[2]/div/div/div/div/ul/li/ul/li[1]/a";
	public static String signInButton = "html/body/div[2]/div/div/div/div/ul/li/span";
	public static String header = "html/body/div[2]/div/div";
	
	public static String quickenLogo= "html/body/div[2]/div/div/a[1]/div";
	// Whole dropdown, parent locator
	public static String siteSelector = "html/body/div[2]/div/div/div[1]/ul/li";
	// locator for currently selected value in the drop-down
	public static String selectedSiteEntryWithinSiteSelector = "span";
	// Site selector dropdown is divided into two major components:
	// 1. Selected site 2. List of options
	public static String optionSublistWithingSiteSelector = "ul";
	
	public static String productMenu = "html/body/div[2]/div/div/nav/ul/li[1]/a";
	public static String canadaProductMenu = "html/body/div[2]/div/div/nav/ul/li[1]/span";
	
	public static String starterEditionItem = "html/body/div[2]/div/div/nav/ul/li[1]/ul/li[1]/ul/li[1]/a";
	public static String homeAndBusinessItem = "html/body/div[2]/div/div/nav/ul/li[1]/ul/li[1]/a";
	
	
}
