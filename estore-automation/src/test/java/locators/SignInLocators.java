package locators;

public class SignInLocators {

	public static String userIdXPath = ".//*[@id='ius-userid']";
	public static String passwordXPath = ".//*[@id='ius-password']";
	public static String signInButtonXPath = ".//*[@id='ius-sign-in-submit-btn']";
	
	public static String userIdLabel = ".//*[@id='ius-label-userid']";
	public static String passwordLabel = ".//*[@id='ius-label-password']";

}
