package locators;

public class BillingInfoPageLocator {

	public static String firstName = ".//*[@id='edit-first-name']";
	public static String lastName = ".//*[@id='edit-last-name']";
	public static String address = ".//*[@id='js-checkout-billing-address']";
	public static String city = ".//*[@id='edit-city']";
	public static String state = ".//*[@id='edit-state']";
	public static String zipcode = ".//*[@id='edit-zip']";
	public static String continueToPaymentInfoButton = ".//*[@id='edit-submit']";
	
	
}
