package properyreader;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import runtime.RuntimeParameter;
import runtime.RuntimeParameterReader;

public class PropertyReader {

	private static Map<PropertyReaderKey, Properties> propertyMap = new HashMap<PropertyReaderKey, Properties>();

	/**
	 * Returns the value specified for the property having name = propertyName
	 * use the runtime config reader to find the current site and check if the
	 * param is specified in site specific property files.
	 * 
	 * @param configProperty
	 * @return
	 */
	public static String getValue(ConfigProperty configProperty) {
		// check for site specific configs
		String site = RuntimeParameterReader
				.getRuntimeParamValue(RuntimeParameter.site);
		String env = RuntimeParameterReader
				.getRuntimeParamValue(RuntimeParameter.environment);
		
		PropertyReaderKey key = new PropertyReaderKey(site, env);

		// add site+env specific config in the propertyMap
		if (!propertyMap.containsKey(key)) {
			addAPropertyFile(key);
		}
		
		return propertyMap.get(key).getProperty(configProperty.name(), configProperty.defualtValue);
	}

	/**
	 * reference: https://www.mkyong.com/java/java-properties-file-examples/
	 * 
	 * @param pathToPropertyFile
	 * @param propertyFileName
	 */
	public static void addAPropertyFile(PropertyReaderKey propertyReaderKey) {
		Properties prop = new Properties();
		InputStream input = null;

		// load a properties file
		try {
			input = new FileInputStream(System.getProperty("user.dir")
					+ "/src/test/resources/config/"
					+ propertyReaderKey.getEnv() + "/"
					+ propertyReaderKey.getSite() + ".properties");
			prop.load(input);
			propertyMap.put(propertyReaderKey, prop);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
