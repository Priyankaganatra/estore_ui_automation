package properyreader;

public enum ConfigProperty {

	HomePageURL("https://qkn:apple@quickenpreprod.prod.acquia-sites.com"), 
	ChangingProperty("default changing prop"), 
	SiteNameInDropdown("United States");
	
	String defualtValue;
	
	ConfigProperty(String value) {
		this.defualtValue = value;
	}
}
