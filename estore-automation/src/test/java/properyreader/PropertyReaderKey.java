package properyreader;

public class PropertyReaderKey {
	
	// Private members and no setter methods for immutability 
	private String site;
	private String env;
	
	
	public PropertyReaderKey(String site, String env) {
		this.site = site;
		this.env = env;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((env == null) ? 0 : env.hashCode());
		result = prime * result + ((site == null) ? 0 : site.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PropertyReaderKey other = (PropertyReaderKey) obj;
		if (env == null) {
			if (other.env != null)
				return false;
		} else if (!env.equals(other.env))
			return false;
		if (site == null) {
			if (other.site != null)
				return false;
		} else if (!site.equals(other.site))
			return false;
		return true;
	}

	public String getSite() {
		return site;
	}

	public String getEnv() {
		return env;
	}

}
