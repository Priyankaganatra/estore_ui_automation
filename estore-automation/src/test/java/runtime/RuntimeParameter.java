package runtime;

import util.Environment;
import util.Site;

public enum RuntimeParameter {
	site("site", Site.US.name), browser("browser", "chrome"), browserDriverLocation(
			"browserDriverLocation", System.getProperty("user.dir")
					+ "/src/test/resources/chromedriver"), environment("environment", Environment.PROD.name);

	public String paramName;
	public String defaultValue;

	RuntimeParameter(String paramName, String defaultValue) {
		this.paramName = paramName;
		this.defaultValue = defaultValue;
	}
}
