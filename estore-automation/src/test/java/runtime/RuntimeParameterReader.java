package runtime;

public class RuntimeParameterReader {

	// Read parameter specified in test-suite.xml and in runtime arguments and
	// -D

	/**
	 * CommandLine Value gets more preference over testng parameter TODO finish
	 * the testng parameters here, i.e pass ITestContext in this method and see
	 * if the param exists in the TestContext, if the param is not specified on
	 * the command line but is available in test-context then use that
	 * 
	 * @param runtimeParam
	 * @return
	 */
	public static String getRuntimeParamValue(RuntimeParameter runtimeParam) {
		return System.getProperty(runtimeParam.paramName, runtimeParam.defaultValue);
	}
}
