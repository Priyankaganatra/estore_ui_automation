package util;

public enum Environment {
	TEST("test"), PROD("prod");
	
	public String name;
	
	Environment(String name) {
		this.name = name;
	}
}
