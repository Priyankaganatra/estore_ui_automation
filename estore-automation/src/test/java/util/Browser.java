package util;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class Browser {
	
	public static RemoteWebDriver getDriver(BrowserName browser) throws MalformedURLException {
		return new RemoteWebDriver(new URL("http://192.168.0.27:4444/wd/hub"), getBrowserCapabilities(browser));
	}
	
	private static DesiredCapabilities getBrowserCapabilities(BrowserName browserName) {
		switch (browserName) {
		case Mozilla:
			System.out.println("Opening firefox driver");
			return DesiredCapabilities.firefox();
		case Chrome:
			System.out.println("Opening chrome driver");
			return DesiredCapabilities.chrome();
		case IE:
			System.out.println("Opening IE driver");
			return DesiredCapabilities.internetExplorer();
		default:
			System.out.println("browser : " + browserName + " is invalid, Launching Firefox as browser of choice..");
			return DesiredCapabilities.firefox();
		}
	}
}
