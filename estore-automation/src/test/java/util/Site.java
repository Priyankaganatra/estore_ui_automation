package util;

public enum Site {
	US("US"), CANADA("Canada");
	
	public String name;
	
	Site(String name) {
		this.name = name;
	}
}
