package util;

import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Utility {

	public static boolean isWebElementPresent(WebDriver driver, By locator) {
		
		if (null == driver || null == locator) {
			throw new IllegalArgumentException("Driver or locator cannot be null");
		}
		try {
			driver.findElement(locator);
		} catch (NoSuchElementException noSuchElementException) {
			return false;
		}
		return true;
	}
}
